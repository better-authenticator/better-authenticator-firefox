document.addEventListener('DOMContentLoaded', function() {

    // Create QR Code with generated channel, password and browser info
    var chanel = makeid(20)
    var password = makeid(20)
    var qrinfo = "baut:" + chanel + "?p=" + password + "&i=" + fnBrowserDetect() + " - " + navigator.oscpu
    new QRCode(document.getElementById("qrcode"), {text: qrinfo, width: 200, height: 200});

    // Initialize the API connection.
    console.log("Initializing connection on:")
    // var socket = io.connect('ws://192.168.0.39:8088/')
    console.log("wss://betterauthenticator-server.onrender.com:443/")
    var socket = io.connect('wss://betterauthenticator-server.onrender.com:443/')
    socket.emit('set_room', {room : chanel})

    // Handles all the messages coming in from server.
    socket.on("tocomputer", (data) => {
        // On receiving a message
        console.log("got a message:")
        console.log(data)
        var decrypted = CryptoJS.AES.decrypt(data.message, password);
        console.log(decrypted)
        var demessage = decrypted.toString(CryptoJS.enc.Utf8)
        console.log(demessage)

        // Message is connection message
        if (demessage.substring(0,9) == "connected") {
            // Save sent data to storage
            chrome.storage.local.set({ "password": password, "chanel": chanel, "computerList": JSON.parse(demessage.substring(10,)) }, function(){});

            window.location.href = '/post.html';
        }
    })

}, false);

// Create random string of characters A-Z a-z 0-9
function makeid(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

// Detect which browser is being used
function fnBrowserDetect(){
  let userAgent = navigator.userAgent;
  let browserName;
  if (navigator.brave) {
    browserName = "Brave";
  } else if (userAgent.match(/chrome|chromium|crios/i)) {
    browserName = "Chrome";
  } else if (userAgent.match(/firefox|fxios/i)) {
    browserName = "Firefox";
  } else if (userAgent.match(/safari/i)) {
    browserName = "Safari";
  } else if (userAgent.match(/opr\//i)) {
    browserName = "Opera";
  } else if (userAgent.match(/edg/i)) {
    browserName = "Edge";
  } else {
    browserName="Unknown";
  }
  return browserName;
}

