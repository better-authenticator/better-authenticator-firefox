
console.log("Autofilling!")

// Possible labels for TOTP input
var names = new RegExp("code|mfa|totp", "i")

/* Loop all inputs to find OPT input */
inputs = document.getElementsByTagName("input") 
for (var inp of inputs) {
    // Skip non OTP/hidden inputs
    if (inp.type == "hidden" || inp.type == "button" || inp.offsetParent == null) {continue;}

    // Detect OTP input
    if (   inp.maxLength == 6
        || inp.type == "tel"
        || names.test(inp.id)
        || names.test(inp.name)
        || names.test(inp.placeholder))
        {
        console.log("Its an OTP!")
        // Fill the value
        inp.value = betterauthenticator_totp;
        // Makes it so that the form detects the input
        inp.dispatchEvent(new Event("input", { bubbles: true }));
    }
}
